package com.huyf.quiz.twmall.controller;

import com.huyf.quiz.twmall.dao.ProductRepository;
import com.huyf.quiz.twmall.entity.Product;
import com.huyf.quiz.twmall.utils.JsonUtil;
import com.huyf.quiz.twmall.web.IntegrationTestBase;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.math.BigDecimal;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @Author: huyf
 * @Date: 2019-08-06 11:30
 */
class ProductControllerTest extends IntegrationTestBase {


    @Autowired
    private ProductRepository productRepository;

    @Test
    void should_return_201_and_message_when_create_product_success() throws Exception {

        Product product = new Product(
                "电脑",
                new BigDecimal(33.22),
                "元",
                "http://www.google.com/image"
        );

        getMockMvc().perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtil.obj2String(product)))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.name", Matchers.is("电脑")))
                .andExpect(jsonPath("$.price", Matchers.is(33.22)))
                .andExpect(jsonPath("$.unit", Matchers.is("元")))
                .andExpect(jsonPath("$.imageUrl", Matchers.is("http://www.google.com/image")));
    }


    @Test
    void should_return_1_and_message_when_create_product_name_exist() throws Exception {

        Product product = new Product(
                "电脑",
                new BigDecimal(33.22),
                "元",
                "http://www.google.com/image"
        );

        productRepository.saveAndFlush(product);

        Product anotherProduct = new Product(
                "电脑",
                new BigDecimal(33.22),
                "元",
                "http://www.google.com/"
        );

        getMockMvc().perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtil.obj2String(product)))
                .andExpect(jsonPath("$.status", Matchers.is(1)))
                .andExpect(jsonPath("$.msg", Matchers.is("商品名称已存在")));
    }


    @Test
    void should_return_200_and_size_is_2_when_get_products() throws Exception {

        Product product = new Product(
                "电脑",
                new BigDecimal(33.22),
                "元",
                "http://www.google.com/image"
        );

        productRepository.saveAndFlush(product);

        Product anotherProduct = new Product(
                "笔记本",
                new BigDecimal(55.34),
                "元",
                "http://www.baidu.com/"
        );

        productRepository.saveAndFlush(anotherProduct);

        getMockMvc().perform(get("/api/products"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name", Matchers.is("电脑")))
                .andExpect(jsonPath("$[1].name", Matchers.is("笔记本")));

    }


















}