package com.huyf.quiz.twmall.controller;

import com.huyf.quiz.twmall.dao.OrderRepository;
import com.huyf.quiz.twmall.dao.ProductRepository;
import com.huyf.quiz.twmall.entity.Order;
import com.huyf.quiz.twmall.entity.Product;
import com.huyf.quiz.twmall.utils.JsonUtil;
import com.huyf.quiz.twmall.web.IntegrationTestBase;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @Author: huyf
 * @Date: 2019-08-06 22:35
 */
class OrderControllerTest extends IntegrationTestBase {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Test
    void should_return_order_name_and_201_when_create_order_success() throws Exception {

        Product product = new Product(
                "可乐",
                new BigDecimal(5.5),
                "元",
                "http://www.baidu.com/"
        );

        productRepository.saveAndFlush(product);

        getMockMvc().perform(post("/api/orders").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtil.obj2String(product)))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.name", Matchers.is("可乐1")));
    }


    @Test
    void should_get_order_list_and_200_when_get_success() throws Exception {

        Product product = new Product(
                "可乐",
                new BigDecimal(5.5),
                "元",
                "http://www.baidu.com/"
        );

        productRepository.saveAndFlush(product);

        Order order = new Order(product.getName());
        order.setProduct(product);

        Order anotherOrder = new Order(product.getName());
        anotherOrder.setProduct(product);


        orderRepository.saveAndFlush(order);
        orderRepository.saveAndFlush(anotherOrder);

        getMockMvc().perform(get("/api/orders"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name", Matchers.is("可乐")))
                .andExpect(jsonPath("$[0].count", Matchers.is(2)))
                .andExpect((jsonPath("$.size()", Matchers.is(1))));
    }


    @Test
    void should_remove_orders_success() throws Exception {

        Product product = new Product(
                "可乐",
                new BigDecimal(5.5),
                "元",
                "http://www.baidu.com/"
        );

        productRepository.saveAndFlush(product);

        Order order = new Order(product.getName());
        order.setProduct(product);

        orderRepository.saveAndFlush(order);

        getMockMvc().perform(delete("/api/orders/1"))
                .andExpect(status().is(200));

        List<Object> orders = orderRepository.findAllByProductId();

        assertEquals(0, orders.size());
    }
}