
CREATE TABLE product_table(
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL ,
  price DECIMAL(6, 2) NOT NULL ,
  unit VARCHAR(10) NOT NULL ,
  image_url VARCHAR(255) NOT NULL
) ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;
