package com.huyf.quiz.twmall.common;

/**
 * @Author: huyf
 * @Date: 2019-08-06 13:46
 */
public enum ResponseCode {

    // todo: no need to create ResponseCode
    SUCCESS(0, "SUCCESS"),
    ERROR(1, "ERROR"),
    NEED_LOGIN(10, "NEED_LOGIN"),
    ILLEGAL_ARGUMENT(2, "ILLEGAL_ARGUMENT");

    private final int code;
    private final String desc;

    ResponseCode(int code, String desc){

        this.code = code;
        this.desc = desc;
    }

    public int getCode(){

        return code;
    }

    public String getDesc(){

        return desc;
    }

}
