package com.huyf.quiz.twmall.dao;

import com.huyf.quiz.twmall.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.Optional;

/**
 * @Author: huyf
 * @Date: 2019-08-06 13:34
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {


    Optional<Product> findProductByName(String name);
}
