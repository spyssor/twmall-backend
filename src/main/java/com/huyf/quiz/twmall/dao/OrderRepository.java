package com.huyf.quiz.twmall.dao;

import com.huyf.quiz.twmall.contract.CreateOrderResponse;
import com.huyf.quiz.twmall.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @Author: huyf
 * @Date: 2019-08-06 13:35
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query(nativeQuery = true, value = "SELECT p.id, p.name, p.price, COUNT(o.product_id) AS count, p.unit " +
                                    "FROM order_table o INNER JOIN product_table p " +
                                    "ON o.product_id = p.id " +
                                    "GROUP BY o.product_id " +
                                    "ORDER BY o.product_id")
    List<Object> findAllByProductId();

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "DELETE FROM order_table WHERE product_id = ?1")
    void deleteByProductId(Long id);
}
