package com.huyf.quiz.twmall.contract;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Author: huyf
 * @Date: 2019-08-06 11:21
 */
public class CreateProductRequest {

    @NotNull
    private String name;

    @NotNull
    @Digits(integer = 6, fraction = 2)
    private BigDecimal price;

    @NotNull
    private String unit;

    @NotNull
    private String imageUrl;


    public CreateProductRequest() {
    }


    public CreateProductRequest(@NotNull String name, @NotNull @Digits(integer = 6, fraction = 2) BigDecimal price,
                                @NotNull String unit, @NotNull String imageUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }

}
