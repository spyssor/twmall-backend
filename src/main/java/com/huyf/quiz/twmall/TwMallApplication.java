package com.huyf.quiz.twmall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwMallApplication {

    public static void main(String[] args) {
        SpringApplication.run(TwMallApplication.class, args);
    }

}
