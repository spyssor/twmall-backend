package com.huyf.quiz.twmall.service;

import com.huyf.quiz.twmall.contract.CreateOrderResponse;
import com.huyf.quiz.twmall.dao.OrderRepository;
import com.huyf.quiz.twmall.dao.ProductRepository;
import com.huyf.quiz.twmall.entity.Order;
import com.huyf.quiz.twmall.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

// todo: no docs
/**
 * @Author: huyf
 * @Date: 2019-08-06 22:35
 */
@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;


    public ResponseEntity addOrder(Product product) {

        Optional<Product> productByName = productRepository.findProductByName(product.getName());

        // todo: the logic has problem
        if (productByName.isPresent()) {
            Order order = new Order(product.getName()+productByName.get().getId());

            order.setProduct(productByName.get());

            Order save = orderRepository.saveAndFlush(order);

            if (save != null) {
                return ResponseEntity.status(HttpStatus.CREATED).body(save);
            }
        }

        return ResponseEntity.badRequest().build();
    }

    public ResponseEntity getOrders() {

        List<Object> objects = orderRepository.findAllByProductId();

        if (objects != null && objects.size() != 0) {
            List<CreateOrderResponse> result = new ArrayList<>();
            objects.forEach(data -> {
                CreateOrderResponse orderResponse = new CreateOrderResponse();
                Object[] object = (Object[]) data;
                Long id = Long.parseLong(object[0].toString());
                orderResponse.setId(id);
                orderResponse.setName((String) object[1]);
                orderResponse.setPrice((BigDecimal) object[2]);
                int count = Integer.parseInt(object[3].toString());
                orderResponse.setCount(count);
                orderResponse.setUnit((String) object[4]);
                result.add(orderResponse);
            });

            return ResponseEntity.ok(result);
        }

        return ResponseEntity.ok(null);
    }

    public ResponseEntity removeOrder(Long orderId) {

        try{
            orderRepository.deleteByProductId(orderId);
        }catch (Exception e){
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().build();
    }
}
