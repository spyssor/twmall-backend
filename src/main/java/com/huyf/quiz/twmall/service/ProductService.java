package com.huyf.quiz.twmall.service;

import com.huyf.quiz.twmall.common.ServerResponse;
import com.huyf.quiz.twmall.dao.ProductRepository;
import com.huyf.quiz.twmall.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @Author: huyf
 * @Date: 2019-08-06 10:47
 */
@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public ResponseEntity createProduct(Product product) {
        Optional<Product> productByName = productRepository.findProductByName(product.getName());

        if (productByName.isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    ServerResponse.createByErrorMessage("商品名称已存在"));
        }

        // todo: bad naming
        Product save = productRepository.save(product);
        if (save != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(save);
        }

        return ResponseEntity.badRequest().build();
    }

    public ResponseEntity findProducts() {

        // todo: create response entity better in controller
        List<Product> products = productRepository.findAll();

        if (products.size() != 0) {
            return ResponseEntity.ok(products);
        }

        return ResponseEntity.badRequest().build();
    }
}
