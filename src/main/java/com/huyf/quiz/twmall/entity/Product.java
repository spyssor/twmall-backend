package com.huyf.quiz.twmall.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @Author: huyf
 * @Date: 2019-08-06 10:43
 */
@Entity
@Table(name = "product_table")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private BigDecimal price;

    @Column(nullable = false)
    private String unit;

    @Column(name = "image_url",nullable = false)
    private String imageUrl;

    public Product() {
    }

    public Product(String name, BigDecimal price, String unit, String imageUrl) {
        this.name = name;
        this.price = price.setScale(2, RoundingMode.HALF_UP);
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }


}
