package com.huyf.quiz.twmall.controller;

import com.huyf.quiz.twmall.contract.CreateProductRequest;
import com.huyf.quiz.twmall.entity.Product;
import com.huyf.quiz.twmall.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Author: huyf
 * @Date: 2019-08-06 10:26
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/products")
    public ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest productRequest) {

        // todo: product creation can be in service
        Product product = new Product(productRequest.getName(), productRequest.getPrice(),
                productRequest.getUnit(), productRequest.getImageUrl());

        return productService.createProduct(product);
    }


    @GetMapping("/products")
    public ResponseEntity createProduct() {

        return productService.findProducts();
    }

}
