package com.huyf.quiz.twmall.controller;

import com.huyf.quiz.twmall.contract.CreateProductRequest;
import com.huyf.quiz.twmall.entity.Product;
import com.huyf.quiz.twmall.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: huyf
 * @Date: 2019-08-06 22:35
 */
@Controller
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/orders")
    public ResponseEntity addOrder(@RequestBody CreateProductRequest productRequest) {



        Product product = new Product(productRequest.getName(), productRequest.getPrice(), productRequest.getUnit(), productRequest.getImageUrl());

        return orderService.addOrder(product);
    }


    @GetMapping("/orders")
    public ResponseEntity getOrders() {

        return orderService.getOrders();
    }

    @DeleteMapping("/orders/{orderId}")
    public ResponseEntity removeOrder(@PathVariable Long orderId) {

        return orderService.removeOrder(orderId);
    }
}
